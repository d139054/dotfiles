# vim:foldmethod=marker:foldlevel=0
# Prompt
autoload -U colors && colors
PS1="%B %{$fg[yellow]%}%c %{$fg[blue]%}>%b "

# History in cache directory:
setopt share_history
HISTFILE="$XDG_STATE_HOME"/zsh/history
HISTSIZE=10000 
SAVEHIST=10000
 
# Autotab Completion
autoload -U compinit
zstyle ':completion:*' menu select
zmodload zsh/complist
compinit
_comp_options+=(globdots)  #Tab complete includes hidden files

# Make shell case-insensitive
zstyle ':completion:*' matcher-list 'm:{a-zA-Z}={A-Za-z}'
setopt nocaseglob

# Colored ls
alias ls='ls -A --color --group-directories-first'
LS_COLORS='di=1;33:fi=0:ln=31:pi=5:so=5:bd=5:cd=5:or=31:mi=0:ex=35:*.rpm=90:*.png=35:*.gif=36:*.jpg=35:*.c=92:*.jar=33:*.py=93:*.h=90:*.txt=94:*.doc=104:*.docx=104:*.odt=104:*.csv=102:*.xlsx=102:*.xlsm=102:*.rb=31:*.cpp=92:*.sh=92:*.html=96:*.zip=4;33:*.tar.gz=4;33:*.mp4=105:*.mp3=106'
export LS_COLORS

# Use vim keys in tab complete menu:
bindkey -M menuselect 'h' vi-backward-char
bindkey -M menuselect 'j' vi-down-line-or-history
bindkey -M menuselect 'k' vi-up-line-or-history
bindkey -M menuselect 'l' vi-forward-char

# Edit text in prompt in vim with ctrl-e
autoload edit-command-line; zle -N edit-command-line
bindkey '^e' edit-command-line

# See https://wiki.archlinux.org/title/Zsh#Key_bindings for more info
typeset -g -A key

key[Delete]="${terminfo[kdch1]}"

[[ -n "${key[Delete]}"    ]] && bindkey -- "${key[Delete]}"     delete-char

bindkey '^R' history-incremental-search-backward

alias v="nvim"
alias q="exit"
alias diamond="cat /dev/urandom"
alias mvi='mpv --config-dir=$HOME/.config/mvi'
alias wget="wget --hsts-file='$XDG_CACHE_HOME/wget-hsts'"
alias gitb="/usr/bin/git --git-dir=$HOME/dtfs/ --work-tree=$HOME"
alias logout="pkill wayland"
# alias anki="~/./bin/pyenv/bin/anki"
# alias nping="$HOME/./scripts/ip-adress"
# alias alass="alass-cli"
# alias vimv="$HOME/./scripts/vimv"

# Change cursor shape for different vi modes.
function zle-keymap-select () {
    case $KEYMAP in
        vicmd) echo -ne '\e[1 q';;      # block
        viins|main) echo -ne '\e[5 q';; # beam
    esac
}
zle -N zle-keymap-select
zle-line-init() {
    zle -K viins # initiate `vi insert` as keymap (can be removed if `bindkey -V` has been set elsewhere)
    echo -ne "\e[5 q"
}

zle -N zle-line-init
echo -ne '\e[5 q' # Use beam shape cursor on startup.
preexec() { echo -ne '\e[5 q' ;} # Use beam shape cursor for each new prompt.
