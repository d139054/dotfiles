[global]
### Display ###
monitor = 0
follow = mouse

# [{width}]x{height}[+/-{x}+/-{y}]
geometry = "300x50-26+40"
indicate_hidden = yes
shrink = yes

# The transparency of the window.  Range: [0; 100]
transparency = 0

notification_height = 0
separator_height = 1
padding = 18
horizontal_padding = 16
frame_width = 0
frame_color = "#D8DEE9"

# possible values are:
#  * auto: dunst tries to find a color fitting to the background;
#  * foreground: use the same color as the foreground;
#  * frame: use the same color as the frame;
#  * anything else will be interpreted as a X color.
separator_color = frame

# Sort messages by urgency.
sort = yes

idle_threshold = 90

### Text ###

font = Rubik 12
line_height = 4

# Possible values are:
# full: Allow a small subset of html markup in notifications:
#		<b>bold</b>
#		<i>italic</i>
#		<s>strikethrough</s>
#		<u>underline</u>
#
#		For a complete reference see
#		<http://developer.gnome.org/pango/stable/PangoMarkupFormat.html>.
#
# strip: This setting is provided for compatibility with some broken
#		clients that send markup even though it's not enabled on the
#		server. Dunst will try to strip the markup but the parsing is
#		simplistic so using this option outside of matching rules for
#		specific applications *IS GREATLY DISCOURAGED*.
#
# no:	Disable markup parsing, incoming notifications will be treated as
#		plain text. Dunst will not advertise that it has the body-markup
#		capability if this is set as a global setting.
#
# It's important to note that markup inside the format option will be parsed
# regardless of what this is set to.
markup = full

# The format of the message.  Possible variables are:
#   %a  appname
#   %s  summary
#   %b  body
#   %i  iconname (including its path)
#   %I  iconname (without its path)
#   %p  progress value if set ([  0%] to [100%]) or nothing
#   %n  progress value if set without any extra characters
# Markup is allowed
format = "%s %p\n%b"

# Possible values are "left", "center" and "right".
alignment = left

# Set to -1 to disable.
show_age_threshold = 60

# Split notifications into multiple lines if they don't fit into
# geometry.
word_wrap = yes

# Ignore newlines '\n' in notifications.
ignore_newline = no

# Merge multiple notifications with the same content
stack_duplicates = true

# Hide the count of merged notifications with the same content
hide_duplicate_count = false

# Display indicators for URLs (U) and actions (A).
show_indicators = yes

### Icons ###

# Align icons left/right/off
icon_position = left

show_icons = left

# Scale larger icons down to this size, set to 0 to disable
max_icon_size = 64

# Paths to default icons.
icon_path = $HOME/.icons/NordArc-Icons/16x16/status/:$HOME/.icons/NordArc-Icons/16x16/devices/:$HOME/.icons/NordArc-Icons/16x16/apps/

### History ###

# Should a notification popped up from history be sticky or timeout
# as if it would normally do.
sticky_history = no

# Maximum amount of notifications kept in history
history_length = 20

### Misc/Advanced ###

# Browser for opening urls in context menu.
browser = /usr/bin/firefox -new-tab

# Always run rule-defined scripts, even if the notification is suppressed
always_run_script = true

# Define the title of the windows spawned by dunst
title = Dunst

# Define the class of the windows spawned by dunst
class = Dunst

startup_notification = false
force_xinerama = false

corner_radius = 5

[shortcuts]
# Xev might be helpful to find names for keys.

# Close notification.
close = ctrl+space

# Close all notifications.
close_all = ctrl+shift+space

# Redisplay last message(s).
# On the US keyboard layout "grave" is normally above TAB and left
# of "1". Make sure this key actually exists on your keyboard layout,
# e.g. check output of 'xmodmap -pke'
history = ctrl+Tab

# Context menu.
context = ctrl+shift+period

[Discord]
appname = Discord
format = "Discord\n%s\n%b"

[urgency_low]
background  = "#2E3440"
foreground  = "#D8DEE9"
timeout     = 4
icon	    = "$HOME/.config/dunst/icons/low.svg"

[urgency_normal]
background = "#D8DEE9"
foreground = "#2e3440"
highlight = "#4c566a"
timeout = 4
icon        = "$HOME/.config/dunst/icons/normal.svg" 

[urgency_critical]
background = "#2E3440"
foreground = "#D8DEE9"
timeout = 0
icon        = "$HOME/.config/dunst/icons/critical.svg" 
