" vim:foldmethod=marker:foldlevel=0

" Plugins {{{
" auto-install vim-plug
if empty(glob('~/.config/nvim/autoload/plug.vim'))
  silent !curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  "autocmd VimEnter * PlugInstall
  "autocmd VimEnter * PlugInstall | source $MYVIMRC
endif

call plug#begin('~/.config/nvim/autoload/plugged')

		" For quick navigation in files
		Plug 'unblevable/quick-scope'
		" Plug 'justinmk/vim-sneak'
		Plug 'plasticboy/vim-markdown'
		Plug 'ap/vim-css-color'
		Plug 'arcticicestudio/nord-vim'
		" Plug 'junegunn/limelight.vim
		" Plug 'junegunn/goyo.vim
		" Plug 'vimwiki/vimwiki'
    "Plug 'hrsh7th/nvim-cmp'
    "Plug 'hrsh7th/cmp-buffer'
    "Plug 'hrsh7th/cmp-path'
    "Plug 'hrsh7th/cmp-cmdline'
    "Plug 'saadparwaiz1/cmp_luasnip'
		"Plug 'L3MON4D3/LuaSnip'
		"Plug 'rafamadriz/friendly-snippets'
		"Plug 'neovim/nvim-lspconfig'
		"Plug 'williamboman/nvim-lsp-installer'
		"Plug 'jose-elias-alvarez/null-ls.nvim'
		Plug 'neovim/nvim-lspconfig'
		Plug 'williamboman/nvim-lsp-installer'

call plug#end()

" }}}

" Plugin Configs {{{

" Quick-scope {{{
" Trigger a highlight in the appropriate direction when pressing these keys:
let g:qs_highlight_on_keys = ['f', 'F', 't', 'T']

highlight QuickScopePrimary guifg='#00C7DF' gui=underline ctermfg=155 cterm=underline
highlight QuickScopeSecondary guifg='#afff5f' gui=underline ctermfg=81 cterm=underline

let g:qs_max_chars=150
" }}}

" Colorscheme {{{
colorscheme nord
" }}}

" Table Mode {{{
let b:table_mode_corner = '|'
let b:table_mode_corner_corner = '|'
let b:table_mode_header_fillchar = '-'
" }}}

" Vim-Markdown {{{
let g:vim_markdown_folding_disabled = 1
let g:vim_markdown_folding_level = 2
set foldenable
" }}}

" }}}

" General {{{
syntax enable
syntax on
set nocompatible
set noerrorbells
set smartindent
set number relativenumber
set pumheight=10
set ruler
set iskeyword+=-
set incsearch
set ignorecase
set smartcase
set mouse=a
set termguicolors
set wildmode=longest,list,full
set tabstop=2
set shiftwidth=2
set autoindent
set cursorline
set showmode
set background=dark
set spell
set spelllang=en_us
set hls is
set ic
set clipboard+=unnamedplus
au! BufWritePost $MYVIMRC source %
filetype plugin on
" autocmd InsertEnter * norm zz
" }}}

" Keybindings {{{
let mapleader=","
map <C-s> :%s//gI<Left><Left><Left>
inoremap <C-u> <ESC>viwUi
nnoremap <C-S-u> viwU<Esc>
vnoremap < <gv
vnoremap > >gv
inoremap jk <Esc>
inoremap kj <Esc>
noremap <silent> <C-p> :Files<CR>
map <F6> :setlocal spell! spelllang=en_us<CR>
map <F7> :set spelllang=es<CR>
" }}}

" Section Folding {{{
set foldenable
set foldlevelstart=10
set foldnestmax=10
set foldmethod=syntax
nnoremap <silent> <Space> @=(foldlevel('.')?'za':"\<Space>")<CR>
vnoremap <Space> za
" }}}

" Word Count {{{

let g:word_count=wordcount().words
function WordCount()
    if has_key(wordcount(),'visual_words')
        let g:word_count=wordcount().visual_words."/".wordcount().words " count selected words
    else
        let g:word_count=wordcount().cursor_words."/".wordcount().words " or shows words 'so far'
    endif
    return g:word_count
endfunction

set statusline+=\ w:%{WordCount()},
set laststatus=2 " show the statusline

" }}}

autocmd BufWritePost *note-*.md silent !buildNote %:p
