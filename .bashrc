HISTSIZE=10000 # Infinite history.

export PS1=" \[$(tput sgr0)\]\[$(tput bold)\]\[\033[38;5;11m\]\W\[$(tput sgr0)\] \[$(tput sgr0)\]\[$(tput bold)\]\[\033[38;5;12m\]>\[$(tput sgr0)\] \[$(tput sgr0)\]" # Shell Prompt

alias ls='ls -A --color'

LS_COLORS='di=1;33:fi=0:ln=31:pi=5:so=5:bd=5:cd=5:or=31:mi=0:ex=35:*.rpm=90:*.png=35:*.gif=36:*.jpg=35:*.c=92:*.jar=33:*.py=93:*.h=90:*.txt=94:*.doc=104:*.docx=104:*.odt=104:*.csv=102:*.xlsx=102:*.xlsm=102:*.rb=31:*.cpp=92:*.sh=92:*.html=96:*.zip=4;33:*.tar.gz=4;33:*.mp4=105:*.mp3=106'

export LS_COLORS

# Aliases
alias v="nvim"
alias q="exit"
alias z="zathura" 
alias fm="pcmanfm &"
alias anki="~/./bin/pyenv/bin/anki"
alias clean="clear; ~/./bin/scripts/arco-fetch"
alias wget="wget --hsts-file='$XDG_CACHE_HOME/wget-hsts'"
alias gitb="/usr/bin/git --git-dir=$HOME/dtfs/ --work-tree=$HOME"
alias vc='nvim "$(find $HOME/.bashrc $HOME/.config/i3/config $HOME/.config/nvim/init.vim | fzf)"'

[ -r /usr/share/bash-completion/bash_completion   ] && . /usr/share/bash-completion/bash_completion

# complete -cf sudo # Improved Tab-Complete
