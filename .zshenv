typeset -U PATH path

# Defaults
export BROWSER="firefox"
export COLORTERM="truecolor"
export EDITOR="nvim"
# export IMAGE="sxiv"
# export OPENER="xdg-open"
export READER="zathura"
export TERMINAL="foot"
export PAGER="less"
export VIDEO="mpv"
export VISUAL="nvim"

# XDG paths
export XDG_CACHE_HOME="$HOME/.cache"
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_STATE_HOME="$HOME/.local/state"

# Cleaning Up Home Directory
export CARGO_HOME="$XDG_DATA_HOME"/cargo
export GOPATH="$XDG_DATA_HOME"/go 
export HISTFILE="$XDG_STATE_HOME"/bash/history # Bash 
export HISTFILE="$XDG_STATE_HOME"/zsh/history # ZSH
export INPUTRC="$XDG_CONFIG_HOME"/readline/inputrc
export _JAVA_OPTIONS=-Djava.util.prefs.userRoot="$XDG_CONFIG_HOME"/java
export LESSHISTFILE="-"
export QT_QPA_PLATFORM=wayland
export RUSTUP_HOME="$XDG_DATA_HOME"/rustup
export SCREENRC="$XDG_CONFIG_HOME"/screen/screenrc
export SSB_HOME="$XDG_DATA_HOME"/zoom
export WGETRC="$XDG_CONFIG_HOME"/wgetrc
export XAUTHORITY="$XDG_RUNTIME_DIR"/Xauthority
export XINITRC="$XDG_CONFIG_HOME"/X11/xinitrc
export ZDOTDIR=$HOME/.config/zsh

path=("$HOME/.local/bin" /other/things/in/path "$path[@]")
export PATH
